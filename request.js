const axios = require("./myAxios");

module.exports = {
    createUnifiedRequest: async (data) => {
        try {
            let response = await axios.post(`https://property-api.mallocdata.com/v1/requests`, data, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY}
            });

            return response.data;
        } catch (e) {
            console.log(e);
            return false;
        }
    },

    getRequestByUserID: async (userID) => {
        try {
            let response = await axios.get(`https://property-api.mallocdata.com/v1/requests/users/${userID}`, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY}
            });

            return response.data;
        } catch (e) {
            console.log(e);
            return false;
        }
    },

    deleteRequestByRequestID: async (id, userId) => {
        try {
            let response = await axios.delete(`https://property-api.mallocdata.com/v1/requests/${id}`, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY},
                data: {
                    userId: userId
                }
            });

            return response.data;
        } catch (e) {
            console.log(e);
            return false;
        }
    },

    toggleSubscription: async (id, userId) => {
        try {
            let response = await axios.put(`https://property-api.mallocdata.com/v1/requests/${id}/toggle-subscription`, {
                userId: userId,
            }, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY}
            });

            return response.data;
        } catch (e) {
            console.log(e);
            return false;
        }
    },
};