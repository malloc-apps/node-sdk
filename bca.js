const axios = require("./myAxios");

module.exports = {
    createBCARequestByAddress: async (searchAddress, priority = 0) => {
        try {
            let response = await axios.post(`https://bc-assessment-api.mallocdata.com/v1/requests`, {
                "address": searchAddress,
                "priority": priority,
            }, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY}
            });

            return response.data;
        } catch (e) {
            return false;
        }
    },

    reload: async (sourceId) => {
        try {
            let response = await axios.post(`https://bc-assessment-api.mallocdata.com/v1/bca/reload/`, {
                "sourceId": sourceId,
            }, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY}
            });

            return response.data;
        } catch (e) {
            return false;
        }
    },

    getBCARequestByID: async (requestID) => {
        try {
            let response = await axios.get(`https://bc-assessment-api.mallocdata.com/v1/requests/${requestID}`, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY}
            });

            return response.data;
        } catch (e) {
            console.log(e);
            return false;
        }
    },
    failedBCARequestByID: async (requestID) => {
        try {
            let response = await axios.put(`https://bc-assessment-api.mallocdata.com/v1/requests/${requestID}/failed`,
                {}, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY}
            });

            return response.data;
        } catch (e) {
            console.log(e);
            return false;
        }
    },

    createBCARequestByUnifiedAddressID: async (unifiedAddressId, priority = 0) => {
        try {
            let response = await axios.post(`https://bc-assessment-api.mallocdata.com/v1/requests`, {
                "unifiedAddressId": unifiedAddressId,
                "priority": priority,
            }, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY}
            });

            return response.data;
        } catch (e) {
            console.log(e);
            return false;
        }
    },

    createBCARequestByEncode: async (encode, priority = 0) => {
        try {
            let response = await axios.post(`https://bc-assessment-api.mallocdata.com/v1/requests`, {
                "encode": encode,
                "priority": priority,
            }, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY}
            });

            return response.data;
        } catch (e) {
            console.log(e);
            return false;
        }
    },
}