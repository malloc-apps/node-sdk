const axios = require("./myAxios");

module.exports = {
    getUnifiedAddressByAddress: async (address, force = false) => {
        let addressResponse = await axios.post(`https://address-api.mallocdata.com/v1/address`, {
            address: address,
            force: force
        }, {
            responseType: 'json',
            headers: {'x-api-key': process.env.API_KEY}
        });
        let addressResult = addressResponse.data;
        if (addressResult['status'] === true) {
            return addressResult;
        }

        return false;
    },

    getUnifiedAddressByID: async (id) => {
        try {
            let response = await axios.get(`https://address-api.mallocdata.com/v1/address/${id}`, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY}
            });

            return response.data;
        } catch (e) {
            console.log(e);
            return false;
        }
    },

    autocomplete: async (input) => {
        try {
            let response = await axios.get(`https://address-api.mallocdata.com/v1/address-auto-complete?input=${encodeURIComponent(input)}`, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY}
            });

            return response.data;
        } catch (e) {
            console.log(e);
            return false;
        }
    },
}
