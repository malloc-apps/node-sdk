module.exports = {
    UserAPI: require("./users"),
    RequestAPI: require("./request"),
    AddressAPI: require("./address"),
    ReportAPI: require("./report"),
    PropertyAPI: require("./property"),
    BCAAPI: require("./bca"),
}