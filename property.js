const axios = require("./myAxios");
const httpBuildQuery = require('http-build-query');

module.exports = {
    getPropertyInfo: async (unifiedAddressId) => {
        try {
            let response = await axios.get(`https://property-api.mallocdata.com/v1/properties/${unifiedAddressId}`, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY}
            });

            return response.data;
        } catch (e) {
            console.log(e);
            return false;
        }
    },

    getPropertyPriceHistory: async (unifiedAddressId, search = ["Asking", "Sold", "Estimate", "Assessment"]) => {
        try {
            let obj = {
                search: search,
            };

            let queryString = httpBuildQuery(obj);
            let response = await axios.get(`https://property-api.mallocdata.com/v1/properties/${unifiedAddressId}/prices?${queryString}`, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY}
            });

            return response.data;
        } catch (e) {
            console.log(e);
            return false;
        }
    },
};