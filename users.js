const axios = require("./myAxios");

module.exports = {
    getUserByID: async (id) => {
        try {
            let response = await axios.get(`https://user-api.mallocdata.com/v1/users/${id}`, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY}
            });

            return response.data;
        } catch (e) {
            console.log(e);
            return false;
        }
    },

    getUserByEmail: async (email) => {
        try {
            let response = await axios.get(`https://user-api.mallocdata.com/v1/search/get-user-by-email?email=${encodeURIComponent(email)}`, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY}
            });

            return response.data;
        } catch (e) {
            console.log(e);
            return false;
        }
    },

    getUserByAPIKey: async (apiKey) => {
        try {
            let response = await axios.get(`https://user-api.mallocdata.com/v1/search/get-user-by-api-key?apiKey=${encodeURIComponent(apiKey)}`, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY}
            });

            return response.data;
        } catch (e) {
            console.log(e);
            return false;
        }
    },

    updateUser: async (id, update) => {
        try {
            let response = await axios.put(`https://user-api.mallocdata.com/v1/users/${id}`, update, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY}
            });

            return response.data;
        } catch (e) {
            console.log(e);
            return false;
        }
    },

    createUser: async (email, name = null, password = null, ip = null, lang = null, extra = {}) => {
        try {
            let response = await axios.post(`https://user-api.mallocdata.com/v1/users`, {
                email: email,
                name: name,
                password: password,
                ip: ip,
                lang: lang,
                extra: extra,
            }, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY}
            });

            return response.data;
        } catch (e) {
            console.log(e);
            return false;
        }
    },

    login: async (email, password, ip = null, extra = {}) => {
        try {
            let response = await axios.post(`https://user-api.mallocdata.com/v1/users/login`, {
                email: email,
                password: password,
                ip: ip,
                extra: extra,
            }, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY}
            });

            return response.data;
        } catch (e) {
            console.log(e);
            return false;
        }
    },

    updatePassword: async (id, currentPassword, newPassword, extra = {}) => {
        try {
            let response = await axios.post(`https://user-api.mallocdata.com/v1/users/${id}/update-password`, {
                currentPassword: currentPassword,
                newPassword: newPassword,
                extra: extra,
            }, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY}
            });

            return response.data;
        } catch (e) {
            console.log(e);
            return false;
        }
    },

    resetPassword: async (id, newPassword, extra = {}) => {
        try {
            let response = await axios.post(`https://user-api.mallocdata.com/v1/users/${id}/reset-password`, {
                newPassword: newPassword,
                extra: extra
            }, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY}
            });

            return response.data;
        } catch (e) {
            console.log(e);
            return false;
        }
    },
}