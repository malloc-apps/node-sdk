const axios = require("./myAxios");

module.exports = {
    createUnifiedReport: async (unifiedAddressId, priority = 0) => {
        try {
            let response = await axios.post(`https://property-api.mallocdata.com/v1/reports`, {
                "unifiedAddressId": unifiedAddressId,
                "priority": priority,
            }, {
                responseType: 'json',
                headers: {'x-api-key': process.env.API_KEY}
            });

            return response.data;
        } catch (e) {
            console.log(e);
            return false;
        }
    },
}